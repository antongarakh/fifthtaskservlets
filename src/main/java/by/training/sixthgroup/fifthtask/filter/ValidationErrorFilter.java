package by.training.sixthgroup.fifthtask.filter;

import by.training.sixthgroup.fifthtask.util.Constants;
import by.training.sixthgroup.fifthtask.util.ResourceManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;


/**
 * Created by Anton on 21.07.2016.
 */
@WebFilter("/*")
public class ValidationErrorFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (servletRequest.getServletContext().getAttribute(Constants.ERROR_MESSAGE) != null) {
            String page = ResourceManager.INSTANCE.getPropertyValue(Constants.ERROR_PAGE_PATH);
            RequestDispatcher dispatcher = servletRequest.getRequestDispatcher(page);
            dispatcher.forward(servletRequest, servletResponse);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
