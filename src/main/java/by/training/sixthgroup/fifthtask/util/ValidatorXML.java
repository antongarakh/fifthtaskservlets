package by.training.sixthgroup.fifthtask.util;

import by.training.sixthgroup.fifthtask.exception.InputException;
import by.training.sixthgroup.fifthtask.exception.ValidationException;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

/**
 * Created by Anton on 12.07.2016.
 */
public class ValidatorXML {


    public static boolean checkIfValid(String pathToXML, String pathToXSD) throws ValidationException, InputException {
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        Source xsd = new StreamSource(new File(pathToXSD));
        Source xml = new StreamSource(new File(pathToXML));
        SchemaFactory schemaFactory = SchemaFactory.newInstance(language);
        try {
            Schema schema = schemaFactory.newSchema(xsd);
            Validator validator = schema.newValidator();
            validator.validate(xml);
            return true;
        } catch (SAXException e) {
            throw new ValidationException(xml.getSystemId() + " is NOT valid, reason: ", e);
        } catch (IOException e) {
            throw new InputException("Something went wrong while reading input files.", e);
        }
    }
}
