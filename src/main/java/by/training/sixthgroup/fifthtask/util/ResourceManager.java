package by.training.sixthgroup.fifthtask.util;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Anton on 20.07.2016.
 */
public enum ResourceManager {
    INSTANCE;
    private ResourceBundle resourceBundle;

    ResourceManager() {
        resourceBundle = ResourceBundle.getBundle(Constants.BUNDLE_PATH);
    }

    public void changeResource(Locale locale) {
        resourceBundle = ResourceBundle.getBundle(Constants.BUNDLE_PATH, locale);
    }

    public String getPropertyValue(String key) {
        return resourceBundle.getString(key);
    }
}
