package by.training.sixthgroup.fifthtask.util;

/**
 * Created by Anton on 13.07.2016.
 */
public class Constants {

    public static final String PATH_TO_XML = "/data/computers.xml";
    public static final String PATH_TO_XSD = "/data/computers.xsd";
    public static final String PAGINATION_START = "start";
    public static final String ERROR_PAGE_PATH = "error_page";
    public static final String XML_PARSE_RESULT_PAGE_PATH = "result_page";
    public static final String HOME_PAGE_PATH = "home_page";
    public static final String BUNDLE_PATH = "config";
    public static final String PARSE_COMMAND = "parse";
    public static final String HOME_PAGE_COMMAND = "home";
    public static final String PARSER = "parser";
    public static final String PARSER_TYPE = "parserType";
    public static final String DEVICES = "devices";
    public static final String ERROR_MESSAGE = "errorMessage";
    public static final String VALID_XML_MESSAGE = "validXml";
    public static final String DEVICE = "device";
    public static final String TYPE = "type";
    public static final String NAME = "name";
    public static final String ORIGIN = "origin";
    public static final String PRICE = "price";
    public static final String INPUT_OUTPUT = "input-output";
    public static final String MULTIMEDIA = "multimedia";
    public static final String GROUP = "group";
    public static final String PORT = "port";
    public static final String ID = "id";
    public static final String PERIPHERAL = "peripheral";
    public static final String WITH_COOLER = "withCooler";
    public static final String ENERGY_CONSUMPTION = "energyConsumption";
    public static final String CRITICAL = "critical";
    public static final String ROOT_PATH_PARAM = "rootPath";
    public static final String TARGET_XML_PATH = "/target/data/computers.xml";
    public static final String USER_DIRECTORY = "user.dir";

    private Constants() {
    }

}

