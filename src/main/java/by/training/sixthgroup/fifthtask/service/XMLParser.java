package by.training.sixthgroup.fifthtask.service;

import by.training.sixthgroup.fifthtask.exception.InputException;
import by.training.sixthgroup.fifthtask.exception.ParserException;
import by.training.sixthgroup.fifthtask.model.Devices;
import by.training.sixthgroup.fifthtask.model.ObjectFactory;

/**
 * Created by Anton on 13.07.2016.
 */
public interface XMLParser {

    ObjectFactory objectFactory = new ObjectFactory();

    Devices parse(String pathToXML) throws ParserException, InputException;
}
