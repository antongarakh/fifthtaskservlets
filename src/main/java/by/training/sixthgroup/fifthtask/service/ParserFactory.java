package by.training.sixthgroup.fifthtask.service;

import by.training.sixthgroup.fifthtask.exception.InputException;
import by.training.sixthgroup.fifthtask.exception.ParserException;

/**
 * Created by Anton on 20.07.2016.
 */
public class ParserFactory {
    public static XMLParser getXMLParserInstance(String parserType) throws ParserException, InputException {
        try {
            ParserType type = ParserType.valueOf(parserType.toUpperCase());
            XMLParser parser = null;
            switch (type) {
                case DOM:
                    parser = new ParserDOM();
                    break;
                case SAX:
                    parser = new ParserSAX();
                    break;
                case STAX:
                    parser = new ParserStAX();
                    break;
            }
            return parser;
        } catch (IllegalArgumentException e) {
            throw new InputException();
        }
    }

    private enum ParserType {
        SAX, STAX, DOM
    }
}
