package by.training.sixthgroup.fifthtask.service;

import by.training.sixthgroup.fifthtask.exception.InputException;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by Anton on 15.07.2016.
 */
public class XSLTransformer {

    public void fromXMLtoXML(String pathToNewXML, String pathToOldXML, String pathToXSL) throws InputException, TransformerException {
        try {
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer(new StreamSource(pathToXSL));
            transformer.transform(new StreamSource(pathToOldXML),
                    new StreamResult(new FileOutputStream(pathToNewXML)));
        } catch (FileNotFoundException e) {
            throw new InputException("InputException while XSL transforming.", e);
        }
    }

}
