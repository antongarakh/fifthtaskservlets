package by.training.sixthgroup.fifthtask.service;

import by.training.sixthgroup.fifthtask.exception.InputException;
import by.training.sixthgroup.fifthtask.exception.ParserException;
import by.training.sixthgroup.fifthtask.model.*;
import by.training.sixthgroup.fifthtask.util.Constants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Anton on 13.07.2016.
 */
public class ParserDOM implements XMLParser {


    @Override
    public Devices parse(String pathToXML) throws ParserException, InputException {
        List<Device> deviceList = new LinkedList<>();
        try {
            File xml = new File(pathToXML);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();
            Document doc = documentBuilder.parse(xml);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName(Constants.DEVICE);
            for (int temp = 0; temp < nodeList.getLength(); temp++) {
                Node node = nodeList.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    deviceList.add(parseDevice(element));
                }
            }
        } catch (SAXException | ParserConfigurationException e) {
            throw new ParserException("Exception while parsing xml", e);
        } catch (IOException e) {
            throw new InputException("IOException while reading xml file to parse", e);
        }
        Devices devices = objectFactory.createDevices();
        devices.setDevice(deviceList);
        return devices;
    }

    private Device parseDevice(Element element) {
        Device device = objectFactory.createDevice();
        device.setId(element.getAttribute(Constants.ID));
        device.setName(element.getElementsByTagName(Constants.NAME).item(0).getTextContent());
        device.setPrice(new BigInteger(element.getElementsByTagName(Constants.PRICE).item(0).getTextContent()));
        device.setOrigin(element.getElementsByTagName(Constants.ORIGIN).item(0).getTextContent());
        device.setType(parseType((Element) element.getElementsByTagName(Constants.TYPE).item(0)));
        return device;
    }

    private Type parseType(Element element) {
        Type type = objectFactory.createType();
        type.setPeripheral(Boolean.valueOf(element.getAttribute(Constants.PERIPHERAL)));
        type.setWithCooler(Boolean.valueOf(element.getAttribute(Constants.WITH_COOLER)));
        type.setEnergyConsumption(new BigInteger(element.getAttribute(Constants.ENERGY_CONSUMPTION)));
        NodeList portNodeList = element.getElementsByTagName(Constants.PORT);
        for (int i = 0; i < portNodeList.getLength(); i++) {
            String portValue = portNodeList.item(i).getTextContent();
            type.addPort(Port.fromValue(portValue));
        }
        type.setGroup(parseGroup((Element) element.getElementsByTagName(Constants.GROUP).item(0)));
        return type;
    }

    private Group parseGroup(Element element) {
        Group group = objectFactory.createGroup();
        Element input_output = (Element) element.getElementsByTagName(Constants.INPUT_OUTPUT).item(0);
        Group.InputOutput inputOutput = objectFactory.createGroupInputOutput();
        inputOutput.setCritical(Boolean.valueOf(input_output.getAttribute(Constants.CRITICAL)));
        group.setInputOutput(inputOutput);
        Element multimediaElement = (Element) element.getElementsByTagName(Constants.MULTIMEDIA).item(0);
        Group.Multimedia multimedia = objectFactory.createGroupMultimedia();
        multimedia.setCritical(Boolean.valueOf(multimediaElement.getAttribute(Constants.CRITICAL)));
        group.setMultimedia(multimedia);
        return group;
    }

}
