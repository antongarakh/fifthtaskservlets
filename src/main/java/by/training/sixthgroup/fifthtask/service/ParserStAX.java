package by.training.sixthgroup.fifthtask.service;

import by.training.sixthgroup.fifthtask.exception.InputException;
import by.training.sixthgroup.fifthtask.exception.ParserException;
import by.training.sixthgroup.fifthtask.model.*;
import by.training.sixthgroup.fifthtask.util.Constants;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import static by.training.sixthgroup.fifthtask.model.Group.InputOutput;
import static by.training.sixthgroup.fifthtask.model.Group.Multimedia;

/**
 * Created by Anton on 14.07.2016.
 */
public class ParserStAX implements XMLParser {

    private List<Device> deviceList;
    private Stack<Device> deviceStack;
    private String currentTag;
    private Type type;

    public ParserStAX() {
        deviceStack = new Stack<>();
        deviceList = new LinkedList<>();
    }

    @Override
    public Devices parse(String pathToXML) throws InputException, ParserException {
        deviceStack.clear();
        deviceList.clear();
        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader reader = factory.createXMLStreamReader(new FileReader(pathToXML));
            extractData(reader);
        } catch (IOException e) {
            throw new InputException("IOException while reading xml file to parse", e);
        } catch (XMLStreamException e) {
            throw new ParserException("Something is wrong with XMLStream", e);
        }
        Devices devices = objectFactory.createDevices();
        devices.setDevice(deviceList);
        return devices;
    }

    private void extractData(XMLStreamReader reader) throws XMLStreamException {
        int event;
        while (reader.hasNext()) {
            event = reader.getEventType();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    processStartElement(reader);
                    break;
                case XMLStreamConstants.CHARACTERS:
                    processCharacters(reader);
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    break;
            }
            reader.next();
        }
    }

    private void processStartElement(XMLStreamReader reader) {
        currentTag = reader.getName().getLocalPart();
        switch (currentTag) {
            case Constants.DEVICE:
                Device device = objectFactory.createDevice();
                device.setId(reader.getAttributeValue(0));
                deviceStack.push(device);
                deviceList.add(device);
                break;
            case Constants.TYPE:
                type = objectFactory.createType();
                type.setPeripheral(Boolean.valueOf(reader.getAttributeValue(0)));
                type.setEnergyConsumption(new BigInteger(reader.getAttributeValue(1)));
                type.setWithCooler(Boolean.valueOf(reader.getAttributeValue(2)));
                deviceStack.peek().setType(type);
                break;
            case Constants.GROUP:
                Group group = objectFactory.createGroup();
                type.setGroup(group);
                break;
            case Constants.MULTIMEDIA:
                Multimedia multimedia = objectFactory.createGroupMultimedia();
                multimedia.setCritical(Boolean.valueOf(reader.getAttributeValue(0)));
                type.getGroup().setMultimedia(multimedia);
                break;
            case Constants.INPUT_OUTPUT:
                InputOutput inputOutput = objectFactory.createGroupInputOutput();
                inputOutput.setCritical(Boolean.valueOf(reader.getAttributeValue(0)));
                type.getGroup().setInputOutput(inputOutput);
                break;
        }
    }

    private void processCharacters(XMLStreamReader reader) {
        if (!reader.isWhiteSpace()) {
            switch (currentTag) {
                case Constants.NAME:
                    deviceStack.peek().setName(reader.getText());
                    break;
                case Constants.ORIGIN:
                    deviceStack.peek().setOrigin(reader.getText());
                    break;
                case Constants.PRICE:
                    deviceStack.peek().setPrice(new BigInteger(reader.getText()));
                    break;
                case Constants.PORT:
                    type.addPort(Port.fromValue(reader.getText()));
                    deviceStack.peek().setType(type);
                    break;
            }
        }
    }

}
