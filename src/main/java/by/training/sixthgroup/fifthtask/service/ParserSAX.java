package by.training.sixthgroup.fifthtask.service;

import by.training.sixthgroup.fifthtask.exception.InputException;
import by.training.sixthgroup.fifthtask.exception.ParserException;
import by.training.sixthgroup.fifthtask.model.*;
import by.training.sixthgroup.fifthtask.util.Constants;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Created by Anton on 13.07.2016.
 */
public class ParserSAX extends DefaultHandler implements XMLParser {

    private List<Device> deviceList;
    private Stack<Device> deviceStack;
    private String currentTag;
    private Type type;
    private Group group;

    public ParserSAX() {
        deviceStack = new Stack<>();
        deviceList = new LinkedList<>();
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attrs) {
        currentTag = qName;
        switch (qName) {
            case Constants.DEVICE:
                Device device = objectFactory.createDevice();
                device.setId(attrs.getValue(0));
                deviceStack.push(device);
                deviceList.add(device);
                break;
            case Constants.TYPE:
                type = objectFactory.createType();
                type.setPeripheral(Boolean.valueOf(attrs.getValue(0)));
                type.setEnergyConsumption(new BigInteger(attrs.getValue(1)));
                type.setWithCooler(Boolean.valueOf(attrs.getValue(2)));
                break;
            case Constants.GROUP:
                group = objectFactory.createGroup();
                type.setGroup(group);

                break;
            case Constants.INPUT_OUTPUT:
                Group.InputOutput inputOutput = objectFactory.createGroupInputOutput();
                inputOutput.setCritical(Boolean.valueOf((attrs.getValue(0))));
                group.setInputOutput(inputOutput);
                break;
            case Constants.MULTIMEDIA:
                Group.Multimedia multimedia = objectFactory.createGroupMultimedia();
                multimedia.setCritical(Boolean.valueOf((attrs.getValue(0))));
                group.setMultimedia(multimedia);
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String value = new String(ch, start, length);
        switch (currentTag) {
            case Constants.NAME:
                deviceStack.peek().setName(value);
                break;
            case Constants.ORIGIN:
                deviceStack.peek().setOrigin(value);
                break;
            case Constants.PRICE:
                deviceStack.peek().setPrice(new BigInteger(value));
                break;
            case Constants.PORT:
                type.addPort(Port.fromValue(value));
                deviceStack.peek().setType(type);
                break;

        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        currentTag = "";
    }

    @Override
    public void endDocument() {
    }

    public Devices parse(String pathToXML) throws ParserException, InputException {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            parser.parse(pathToXML, this);
        } catch (SAXException | ParserConfigurationException e) {
            throw new ParserException("Exception while parsing xml", e);
        } catch (IOException e) {
            throw new InputException("IOException while reading xml file to parse", e);
        }
        Devices devices = objectFactory.createDevices();
        devices.setDevice(deviceList);
        return devices;
    }


}
