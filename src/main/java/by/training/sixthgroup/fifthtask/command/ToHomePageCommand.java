package by.training.sixthgroup.fifthtask.command;

import by.training.sixthgroup.fifthtask.util.Constants;
import by.training.sixthgroup.fifthtask.util.ResourceManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Anton on 20.07.2016.
 */
public class ToHomePageCommand implements Command {
    public String execute(HttpServletRequest request) {
        return ResourceManager.INSTANCE.getPropertyValue(Constants.HOME_PAGE_PATH);
    }
}
