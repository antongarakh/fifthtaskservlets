package by.training.sixthgroup.fifthtask.command;

import by.training.sixthgroup.fifthtask.exception.InputException;
import by.training.sixthgroup.fifthtask.exception.ParserException;
import by.training.sixthgroup.fifthtask.model.Devices;
import by.training.sixthgroup.fifthtask.service.ParserFactory;
import by.training.sixthgroup.fifthtask.service.XMLParser;
import by.training.sixthgroup.fifthtask.util.Constants;
import by.training.sixthgroup.fifthtask.util.ResourceManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Anton on 20.07.2016.
 */
public class ParseXMLCommand implements Command {
    public String execute(HttpServletRequest request) throws ParserException, InputException, CloneNotSupportedException {
        String page;
        String type = request.getParameter(Constants.PARSER).toUpperCase();
        XMLParser parser = ParserFactory.getXMLParserInstance(type);
        String path = request.getServletContext().getRealPath(Constants.PATH_TO_XML);
        String start = request.getParameter(Constants.PAGINATION_START);
        Devices devices = parser.parse(path);
        request.setAttribute(Constants.PARSER_TYPE, type);
        request.setAttribute(Constants.DEVICES, devices.getDevices());
        request.setAttribute(Constants.PAGINATION_START, start);
        page = ResourceManager.INSTANCE.getPropertyValue(Constants.XML_PARSE_RESULT_PAGE_PATH);
        return page;
    }
}
