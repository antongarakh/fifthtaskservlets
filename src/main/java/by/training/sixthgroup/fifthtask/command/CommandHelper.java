package by.training.sixthgroup.fifthtask.command;

import by.training.sixthgroup.fifthtask.util.Constants;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Anton on 20.07.2016.
 */
public class CommandHelper {
    private static CommandHelper instance = null;

    public static CommandHelper getInstance() {
        if (instance == null) {
            instance = new CommandHelper();
        }
        return instance;
    }

    public Command getCommand(HttpServletRequest request) {
        String action = request.getParameter("command");
        Command command;
        if (action == null) {
            return new ToHomePageCommand();
        }
        switch (action) {
            case Constants.HOME_PAGE_COMMAND:
                command = new ToHomePageCommand();
                break;
            case Constants.PARSE_COMMAND:
                command = new ParseXMLCommand();
                break;
            default:
                command = new ToHomePageCommand();
                break;
        }
        return command;
    }
}
