package by.training.sixthgroup.fifthtask.command;

import by.training.sixthgroup.fifthtask.exception.InputException;
import by.training.sixthgroup.fifthtask.exception.ParserException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Anton on 20.07.2016.
 */
public interface Command {
    String execute(HttpServletRequest request) throws ParserException, InputException, CloneNotSupportedException;
}
