package by.training.sixthgroup.fifthtask.controller;

import by.training.sixthgroup.fifthtask.command.Command;
import by.training.sixthgroup.fifthtask.command.CommandHelper;
import by.training.sixthgroup.fifthtask.exception.InputException;
import by.training.sixthgroup.fifthtask.exception.ParserException;
import by.training.sixthgroup.fifthtask.util.Constants;
import by.training.sixthgroup.fifthtask.util.ResourceManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Created by Anton on 19.07.2016.
 */
@WebServlet("/controller")
public class DeviceServlet extends HttpServlet {


    public static final Logger logger = LogManager.getLogger(DeviceServlet.class);


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.info("in doGet");
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("in doPost");
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String page = null;
        try {
            Command command = CommandHelper.getInstance().getCommand(request);
            page = command.execute(request);
            logger.info("Parsing completed successfully");
            RequestDispatcher dispatcher = request.getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            logger.error("ServletException", e);
            request.getServletContext().setAttribute("errorMessage", "ServletException");
            forwardToErrorPage(request, response, page);
        } catch (InputException e) {
            logger.error("InputException", e);
            request.getServletContext().setAttribute("errorMessage", "InputException");
            forwardToErrorPage(request, response, page);
        } catch (ParserException e) {
            logger.error("ParserException", e);
            request.getServletContext().setAttribute("errorMessage", "ParseException");
            forwardToErrorPage(request, response, page);
        } catch (CloneNotSupportedException e) {
            logger.info("CloneNotSupportedException", e);
            request.getServletContext().setAttribute("errorMessage", "CloneNotSupportedException");
            forwardToErrorPage(request, response, page);
        }
    }

    private void forwardToErrorPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
        page = ResourceManager.INSTANCE.getPropertyValue(Constants.ERROR_PAGE_PATH);
        RequestDispatcher dispatcher = request.getRequestDispatcher(page);
        dispatcher.forward(request, response);
    }

}
