package by.training.sixthgroup.fifthtask.exception;

/**
 * Created by Anton on 13.07.2016.
 */
public class ValidationException extends Exception {

    public ValidationException() {

    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationException(Throwable cause) {
        super(cause);
    }

    public ValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
