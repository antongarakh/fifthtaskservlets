package by.training.sixthgroup.fifthtask.exception;

/**
 * Created by Anton on 13.07.2016.
 */
public class ParserException extends Exception {
    public ParserException() {

    }

    public ParserException(String message) {
        super(message);
    }

    public ParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParserException(Throwable cause) {
        super(cause);
    }

    public ParserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
