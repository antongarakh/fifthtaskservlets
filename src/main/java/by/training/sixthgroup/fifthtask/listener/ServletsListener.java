package by.training.sixthgroup.fifthtask.listener;

import by.training.sixthgroup.fifthtask.exception.InputException;
import by.training.sixthgroup.fifthtask.exception.ValidationException;
import by.training.sixthgroup.fifthtask.util.Constants;
import by.training.sixthgroup.fifthtask.util.ValidatorXML;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Created by Anton on 21.07.2016.
 */
@WebListener("/controller")
public class ServletsListener implements ServletContextListener {

    public static final Logger logger = LogManager.getLogger(ServletsListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        System.setProperty(Constants.ROOT_PATH_PARAM, servletContext.getRealPath("/"));
        String pathToXml = servletContext.getRealPath(Constants.PATH_TO_XML);
        String pathToXsd = servletContext.getRealPath(Constants.PATH_TO_XSD);
        try {
            boolean isValidXml = ValidatorXML.checkIfValid(pathToXml, pathToXsd);
            if (isValidXml) {
                servletContext.setAttribute(Constants.VALID_XML_MESSAGE, "XML validation is complete. You are allowed to choose parser.");
                logger.info("Xml is valid");
            }
        } catch (ValidationException | InputException e) {
            servletContext.setAttribute(Constants.ERROR_MESSAGE, "XML validation is incomplete. Further operations are not available.");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
