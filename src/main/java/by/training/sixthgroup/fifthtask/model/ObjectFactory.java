package by.training.sixthgroup.fifthtask.model;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public Group createGroup() {
        return new Group();
    }

    public Devices createDevices() {
        return new Devices();
    }

    public Device createDevice() {
        return new Device();
    }

    public Type createType() {
        return new Type();
    }

    public Group.InputOutput createGroupInputOutput() {
        return new Group.InputOutput();
    }

    public Group.Multimedia createGroupMultimedia() {
        return new Group.Multimedia();
    }

}
