package by.training.sixthgroup.fifthtask.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "device"
})
@XmlRootElement(name = "devices")
public class Devices implements Cloneable {

    @XmlElement(required = true)
    protected List<Device> device;

    @Override
    public Devices clone() throws CloneNotSupportedException {
        Devices copyDevices;
        copyDevices = (Devices) super.clone();
        List<Device> copyDevicesList = new ArrayList<>();
        for (Device deviceFromList : device) {
            copyDevicesList.add(deviceFromList.clone());
        }
        copyDevices.setDevice(copyDevicesList);
        return copyDevices;
    }

    public List<Device> getDevices() throws CloneNotSupportedException {
        if (device == null) {
            device = new ArrayList<>();
        }
        List<Device> copyDevicesList = new ArrayList<>();
        for (Device deviceFromList : device) {
            copyDevicesList.add(deviceFromList.clone());
        }
        return copyDevicesList;
    }

    public void setDevice(List<Device> device) {
        this.device = device;
    }

    @Override
    public String toString() {
        return "Devices{" +
                "device=" + device +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Devices)) return false;

        Devices devices = (Devices) o;

        return !(device != null ? !device.equals(devices.device) : devices.device != null);

    }

    @Override
    public int hashCode() {
        return device != null ? device.hashCode() : 0;
    }
}
