package by.training.sixthgroup.fifthtask.model;

import javax.xml.bind.annotation.*;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Group", propOrder = {
        "inputOutput",
        "multimedia"
})
public class Group implements Cloneable {

    @XmlElement(name = "input-output", required = true)
    protected InputOutput inputOutput;
    @XmlElement(required = true)
    protected Multimedia multimedia;

    @Override
    public Group clone() throws CloneNotSupportedException {
        Group copyGroup;
        copyGroup = (Group) super.clone();
        copyGroup.setMultimedia(multimedia.clone());
        copyGroup.setInputOutput(inputOutput.clone());
        return copyGroup;
    }

    public InputOutput getInputOutput() {
        return inputOutput;
    }

    public void setInputOutput(InputOutput value) {
        this.inputOutput = value;
    }

    public Multimedia getMultimedia() {
        return multimedia;
    }

    public void setMultimedia(Multimedia value) {
        this.multimedia = value;
    }

    @Override
    public String toString() {
        return "Group{" +
                "inputOutput=" + inputOutput +
                ", multimedia=" + multimedia +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;

        Group group = (Group) o;

        return !(getInputOutput() != null ? !getInputOutput().equals(group.getInputOutput()) : group.getInputOutput() != null) && !(getMultimedia() != null ? !getMultimedia().equals(group.getMultimedia()) : group.getMultimedia() != null);

    }

    @Override
    public int hashCode() {
        int result = getInputOutput() != null ? getInputOutput().hashCode() : 0;
        result = 31 * result + (getMultimedia() != null ? getMultimedia().hashCode() : 0);
        return result;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class InputOutput implements Cloneable {

        @XmlAttribute(name = "critical", required = true)
        protected boolean critical;

        @Override
        public InputOutput clone() throws CloneNotSupportedException {
            InputOutput copyInputOutput;
            copyInputOutput = (InputOutput) super.clone();
            return copyInputOutput;
        }

        public boolean isCritical() {
            return critical;
        }

        public void setCritical(boolean value) {
            this.critical = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof InputOutput)) return false;

            InputOutput that = (InputOutput) o;

            return isCritical() == that.isCritical();

        }

        @Override
        public int hashCode() {
            return (isCritical() ? 1 : 0);
        }

        @Override

        public String toString() {
            return "InputOutput{" +
                    "critical=" + critical +
                    '}';
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Multimedia implements Cloneable {

        @XmlAttribute(name = "critical", required = true)
        protected boolean critical;

        @Override
        public Multimedia clone() throws CloneNotSupportedException {
            Multimedia copyMultimedia;
            copyMultimedia = (Multimedia) super.clone();
            return copyMultimedia;
        }

        public boolean isCritical() {
            return critical;
        }

        public void setCritical(boolean value) {
            this.critical = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Multimedia)) return false;

            Multimedia that = (Multimedia) o;

            return isCritical() == that.isCritical();

        }

        @Override
        public int hashCode() {
            return (isCritical() ? 1 : 0);
        }

        @Override
        public String toString() {
            return "Multimedia{" +
                    "critical=" + critical +
                    '}';
        }
    }
}
