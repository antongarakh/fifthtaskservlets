package by.training.sixthgroup.fifthtask.model;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigInteger;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Device", propOrder = {
        "name",
        "origin",
        "price",
        "type"
})
public class Device implements Cloneable {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "Name")
    protected String name;

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "Name")
    protected String origin;

    @XmlElement(required = true)
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger price;

    @XmlElement(required = true)
    protected Type type;

    @XmlAttribute(name = "id", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    protected String id;

    @Override
    public Device clone() throws CloneNotSupportedException {
        Device device;
        device = (Device) super.clone();
        device.setType(type.clone());
        return device;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String value) {
        this.origin = value;
    }

    public BigInteger getPrice() {
        return price;
    }

    public void setPrice(BigInteger value) {
        this.price = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Device)) return false;

        Device device = (Device) o;

        if (getName() != null ? !getName().equals(device.getName()) : device.getName() != null) return false;
        if (getOrigin() != null ? !getOrigin().equals(device.getOrigin()) : device.getOrigin() != null) return false;
        if (getPrice() != null ? !getPrice().equals(device.getPrice()) : device.getPrice() != null) return false;
        return !(getType() != null ? !getType().equals(device.getType()) : device.getType() != null) && !(getId() != null ? !getId().equals(device.getId()) : device.getId() != null);

    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (getOrigin() != null ? getOrigin().hashCode() : 0);
        result = 31 * result + (getPrice() != null ? getPrice().hashCode() : 0);
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        result = 31 * result + (getId() != null ? getId().hashCode() : 0);
        return result;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type value) {
        this.type = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String value) {
        this.id = value;
    }

    @Override
    public String toString() {
        return "Device{" +
                "name='" + name + '\'' +
                ", origin='" + origin + '\'' +
                ", price=" + price +
                ", type=" + type +
                ", id='" + id + '\'' +
                '}';
    }
}
