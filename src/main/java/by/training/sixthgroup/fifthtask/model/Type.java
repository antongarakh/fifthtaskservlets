package by.training.sixthgroup.fifthtask.model;

import javax.xml.bind.annotation.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Type", propOrder = {
        "group",
        "port"
})
public class Type implements Cloneable {

    @XmlElement(required = true)
    protected Group group;


    @XmlSchemaType(name = "string")
    protected List<Port> port;
    @XmlAttribute(name = "peripheral", required = true)
    protected boolean peripheral;
    @XmlAttribute(name = "energyConsumption", required = true)
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger energyConsumption;
    @XmlAttribute(name = "withCooler", required = true)
    protected boolean withCooler;

    @Override
    public Type clone() throws CloneNotSupportedException {
        Type copyType;
        copyType = (Type) super.clone();
        copyType.setGroup(group.clone());
        copyType.setPort(this.getPort());
        return copyType;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group value) {
        this.group = value;
    }

    public void addPort(Port newPort) {
        if (port == null) {
            port = new ArrayList<>();
        }
        port.add(newPort);
    }

    public boolean isPeripheral() {
        return peripheral;
    }

    public void setPeripheral(boolean value) {
        this.peripheral = value;
    }

    public BigInteger getEnergyConsumption() {
        return energyConsumption;
    }

    public void setEnergyConsumption(BigInteger value) {
        this.energyConsumption = value;
    }

    public boolean isWithCooler() {
        return withCooler;
    }

    public void setWithCooler(boolean value) {
        this.withCooler = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Type)) return false;

        Type type = (Type) o;

        if (isPeripheral() != type.isPeripheral()) return false;
        return isWithCooler() == type.isWithCooler() && !(getGroup() != null ? !getGroup().equals(type.getGroup()) : type.getGroup() != null) && !(getPort() != null ? !getPort().equals(type.getPort()) : type.getPort() != null) && !(getEnergyConsumption() != null ? !getEnergyConsumption().equals(type.getEnergyConsumption()) : type.getEnergyConsumption() != null);
    }

    @Override
    public int hashCode() {
        int result = getGroup() != null ? getGroup().hashCode() : 0;
        result = 31 * result + (getPort() != null ? getPort().hashCode() : 0);
        result = 31 * result + (isPeripheral() ? 1 : 0);
        result = 31 * result + (getEnergyConsumption() != null ? getEnergyConsumption().hashCode() : 0);
        result = 31 * result + (isWithCooler() ? 1 : 0);
        return result;
    }

    public List<Port> getPort() {
        return new ArrayList<>(port);
    }

    public void setPort(List<Port> port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "Type{" +
                "group=" + group +
                ", port=" + port +
                ", peripheral=" + peripheral +
                ", energyConsumption=" + energyConsumption +
                ", withCooler=" + withCooler +
                '}';
    }
}
