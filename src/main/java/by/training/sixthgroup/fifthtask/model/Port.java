package by.training.sixthgroup.fifthtask.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "Port")
@XmlEnum
public enum Port {

    USB,
    COM,
    LPT;

    public static Port fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }


}
