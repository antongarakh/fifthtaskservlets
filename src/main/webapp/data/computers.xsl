<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <devices>
            <xsl:apply-templates/>
        </devices>
    </xsl:template>

    <xsl:template match="device">
        <device id="{@id}" name="{name}">
            <origin>
                <xsl:value-of select="origin"/>
            </origin>
            <price>
                <xsl:value-of select="price"/>
            </price>
            <type withCooler="{type/@withCooler}">
                <peripheral>
                    <xsl:value-of select="type/@peripheral"/>
                </peripheral>
                <energyConsumption>
                    <xsl:value-of select="type/@energyConsumption"/>
                </energyConsumption>
            </type>
        </device>
    </xsl:template>

</xsl:stylesheet>