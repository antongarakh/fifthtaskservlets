<%--
  Created by IntelliJ IDEA.
  User: Anton
  Date: 20.07.2016
  Time: 17:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" language="java" isELIgnored="false" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="mt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap.min.css") %>'>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap-theme.min.css") %>'>
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("jquery.min.js") %>'></script>
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("js/bootstrap.min.js") %>'></script>
<meta charset="utf-8">
<head>
    <title>Devices</title>
</head>
<body>
<div class="container">
    <h2>Result of parsing</h2>

    <c:set var="totalCount" scope="session" value="${devices.size()}"/>
    <c:set var="devices" scope="session" value="${devices}"/>
    <c:set var="perPage" scope="session" value="${4}"/>
    <c:set var="pageStart" value="${param.start}"/>
    <c:if test="${empty pageStart or pageStart < 0}">
        <c:set var="pageStart" value="0"/>
    </c:if>
    <c:if test="${totalCount < pageStart}">
        <c:set var="pageStart" value="${pageStart - perPage}"/>
    </c:if>

    <table class="table text-center " border="1">
        <thead class="text-center">
        <tr>
            <th class="text-center" rowspan="3">Device id</th>
            <th class="text-center" rowspan="3">name</th>
            <th class="text-center" rowspan="3">origin</th>
            <th class="text-center" rowspan="3">price</th>
            <th class="text-center" colspan="100%">type</th>
        </tr>
        <tr>
            <th class="text-center" rowspan="2">peripheral</th>
            <th class="text-center" rowspan="2">withCooler</th>
            <th class="text-center" colspan="2">group</th>
            <th class="text-center" rowspan="2" colspan="100%">port</th>
        </tr>
        <tr>
            <th class="text-center">input_output critical</th>
            <th class="text-center">multimedia critical</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${devices}" var="current" begin="${pageStart}" end="${pageStart + perPage - 1}">
            <tr>
                <td><c:out value="${current.id}"/></td>
                <td><c:out value="${current.name}"/></td>
                <td><c:out value="${current.origin}"/></td>
                <td><c:out value="${current.price}"/></td>
                <td><c:out value="${current.type.peripheral}"/></td>
                <td><c:out value="${current.type.withCooler}"/></td>
                <td><c:out value="${current.type.group.inputOutput.critical}"/></td>
                <td><c:out value="${current.type.group.multimedia.critical}"/></td>
                <c:forEach items="${current.type.port}" var="port">
                    <td><c:out value="${port}"/></td>
                </c:forEach>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<div align="center">
    <ul class="pagination">
        <li><a href="controller?command=${param.command}&parser=${param.parser}&start=${pageStart - perPage}"><<</a>
        </li>
        <li class="disabled"><a>${pageStart + 1} - ${pageStart + perPage}</a></li>
        <li><a href="controller?command=${param.command}&parser=${param.parser}&start=${pageStart + perPage}">>></a>
        </li>
    </ul>
</div>
<div align="center">
    <form action="controller" method="get">
        <input type="hidden" name="command" value="home">
        <input type="submit" class="btn btn-default next" value="home">
    </form>
</div>
</body>
</html>
