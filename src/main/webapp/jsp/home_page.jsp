<%--
  Created by IntelliJ IDEA.
  User: Anton
  Date: 20.07.2016
  Time: 17:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" language="java" isELIgnored="false" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="mt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap.min.css") %>'>
<link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap-theme.min.css") %>'>
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("jquery.min.js") %>'></script>
<script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("js/bootstrap.min.js") %>'></script>
<meta charset="utf-8">
<head>
    <title>Devices</title>
</head>
<body>
<h2>${validXml}</h2>

<h2>Choose parser type</h2>

<div class="form-inline col-sm-offset-5 col-sm-3 text-center">
    <form action="controller" method="get" class="form-group">
        <input type="hidden" name="command" value="parse">
        <input type="hidden" name="parser" value="dom">
        <input type="submit" class="btn btn-default center-block " value="DOM">
    </form>
    <form action="controller" method="get" class="form-group">
        <input type="hidden" name="command" value="parse">
        <input type="hidden" name="parser" value="sax">
        <input type="submit" class="btn btn-default center-block " value="SAX">
    </form>
    <form action="controller" method="get" class="form-group">
        <input type="hidden" name="command" value="parse">
        <input type="hidden" name="parser" value="stax">
        <input type="submit" class="btn btn-default center-block" value="STAX">
    </form>
</div>
</body>
</html>

