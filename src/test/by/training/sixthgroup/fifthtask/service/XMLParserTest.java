package by.training.sixthgroup.fifthtask.service;

import by.training.sixthgroup.fifthtask.model.Devices;
import junit.framework.TestCase;

import static by.training.sixthgroup.fifthtask.util.Constants.TARGET_XML_PATH;
import static by.training.sixthgroup.fifthtask.util.Constants.USER_DIRECTORY;

/**
 * Created by Anton on 22.07.2016.
 */
public class XMLParserTest extends TestCase {

    public void testParse() throws Exception {
        String path = System.getProperty(USER_DIRECTORY) + TARGET_XML_PATH;
        Devices devicesDOM = new ParserDOM().parse(path);
        Devices devicesStAX = new ParserStAX().parse(path);
        Devices devicesSAX = new ParserSAX().parse(path);
        boolean areResultsEqual = false;
        if (devicesDOM.equals(devicesSAX) && devicesSAX.equals(devicesStAX)) {
            areResultsEqual = true;
        }
        assertEquals(true, areResultsEqual);
    }

}