package by.training.sixthgroup.fifthtask.filter;

import by.training.sixthgroup.fifthtask.util.Constants;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.*;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Anton on 22.07.2016.
 */


public class ValidationErrorFilterTest extends TestCase {

    @InjectMocks
    Filter errorFilter = new ValidationErrorFilter();
    @Mock
    private FilterChain filterChain;
    @Mock
    private ServletRequest servletRequest;
    @Mock
    private ServletContext servletContext;
    @Mock
    private ServletResponse servletResponse;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testDoFilter() throws Exception {
        when(servletRequest.getServletContext()).thenReturn(servletContext);
        when(servletContext.getAttribute(Constants.ERROR_MESSAGE)).thenReturn(null);
        errorFilter.doFilter(servletRequest, servletResponse, filterChain);
        verify(filterChain).doFilter(servletRequest, servletResponse);
    }
}